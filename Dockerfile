FROM rocker/r-ver:3.2.2
VOLUME /data

RUN chmod a+rwx /data
RUN mkdir /data/mabra_user_data

RUN apt-get update && apt-get install -y \
    sudo \
    gdebi-core \
    pandoc \
    pandoc-citeproc \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libgit2-dev \
    libmagick++-dev \
    libxt-dev \
    libxml2-dev \
    libssl-dev \
    xtail \
    xz-utils \
    wget \
    libglu1-mesa-dev \
    freeglut3-dev \
    mesa-common-dev

#RUN wget --no-verbose http://snapshot.debian.org/archive/debian/20110406T213352Z/pool/main/o/openssl098/libssl0.9.8_0.9.8o-7_amd64.deb -O "libssl.deb" && \
#    dpkg -i libssl.deb 

# Download and install shiny server
COPY resources /home/resources
RUN wget --no-verbose https://download3.rstudio.org/ubuntu-14.04/x86_64/VERSION -O "version.txt" && \
    VERSION=$(cat version.txt)  && \
    wget --no-verbose "https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-$VERSION-amd64.deb" -O ss-latest.deb && \
    gdebi -n ss-latest.deb && \
    rm -f version.txt ss-latest.deb && \
    . /etc/environment

# copy site-library to /usr/local/lib/R/site-library/
RUN unzip /home/resources/site-library.zip && \
    cp -R site-library/ /usr/local/lib/R/

# Downgrade shiny to 1.0.5 same as web version of BRepertoire
#RUN R -e "remove.packages('shiny')"
#RUN R -e "devtools::install_version('shiny', version = '1.0.5', repos = 'http://cran.rstudio.org')"

# Copy over shiny server configurations
# COPY resources/shiny-server.conf /etc/shiny-server/shiny-server.conf

EXPOSE 3838

COPY shiny-server.sh /usr/bin/shiny-server.sh

CMD ["/usr/bin/shiny-server.sh"]
