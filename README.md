# BRepertoire Docker version 

This is a ‘Docker’ version of the BRepertoire server. Using Docker, essentially a ‘Virtual Machine’-like architecture can be used to host web-app like BRepertoire. This freezes software dependencies so that every instance of the web-server can operate using identical versions as the web-servers. The hardware (CPU, RAM) requirements of the computation is subjected to the availability in the desktop/laptop machine on which the Dockerised version is running, rather than limitations imposed by the server which hosts BRepertoire on the web. 

If you use this please cite:

Margreitter, C., Lu, H. C., Townsend, C., Stewart, A., Dunn-Walters, D. K., & Fraternali, F. (2018). BRepertoire: a user-friendly web server for analysing antibody repertoire data. Nucleic acids research, 46(W1), W264–W270. <https://doi.org/10.1093/nar/gky276>

## Requirements

* A functioning version of Docker. Follow https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-engine---community-1 for ubuntu. The website also contains installation instructions for other Linux builds. 

* Docker compose. Follow https://docs.docker.com/compose/install/ for installation instructions. This is used to configure the BRepertoire service appropriately. 

* By default, running Docker requires root (‘sudo’) access to the hardware. Follow this: https://docs.docker.com/install/linux/linux-postinstall/ to change this so that no root privilege is needed to run the hardware. 

## Installation 

1. Clone this repository to your machine. 

2. On the Terminal, enter the directory brepertoiredocker which has just been created. (`cd <path>/<to>/brepertoiredocker` or equivalent)

3. Build the Brepertorie image. Run this on the terminal:  

```     
docker-compose up 
```
     
   This will take around 40 minutes to set up the image and install all dependencies of the BRepertoire server. 

4. Go to your web browser and key in localhost/BRepertoire – you are now running a BRepertoire instance on your machine! 

5. To Stop the service, go back to the Terminal, and press CTRL + C. The prompt should say the shiny service (using which BRepertoire is operating) is stopping. 

6. Next time if you need to run BRepertoire again, go straight to Step 2. 


## To-do and remarks 

1. This dockerised version has also been tested on MacOS. Follow here (https://docs.docker.com/docker-for-mac/install/) to install Docker on your Mac and follow the instructions above to build the Docker image and run BRepertoire. 

2. To-do: In theory this should also be operative on Windows machines with Docker installed. BUT THIS HAS NOT BEEN TESTED YET! Nothing guaranteed! 

 
## Contact 

Joseph Ng 

Randall Centre for Cell and Molecular Biophysics 

King’s College London 

Email:  joseph (dot) ng (at) kcl (dot) ac (dot) uk 
